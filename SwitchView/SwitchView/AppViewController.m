//
//  AppViewController.m
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import "AppViewController.h"

@interface AppViewController ()

@end

@implementation AppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.view.backgroundColor = [UIColor yellowColor];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)switchValueChange:(id)sender {
    
    UISwitch *sw = (UISwitch*)sender;
    
    if(YES == sw.isOn){
    
        self.lblTitle.text = @"Light Currenty On";
        self.lblTitle.textColor = [UIColor blackColor];
        self.view.backgroundColor = [UIColor yellowColor];
        
    }else{
        
        self.lblTitle.text = @"Light Currenty Off";
        self.lblTitle.textColor = [UIColor whiteColor];
        self.view.backgroundColor = [UIColor blackColor];
        
    }
    
}

@end
