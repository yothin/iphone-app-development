//
//  AppViewController.m
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import "AppViewController.h"
#import "MovieTableViewCell.h"

@interface AppViewController ()

@end

@implementation AppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)fetchMovies{
    
    self.dataSource = [[NSMutableArray alloc] init];
    
    // 1
    NSDictionary *movie = [NSDictionary dictionaryWithObjectsAndKeys:
                           @"The Shawshank Redemption (1994)",@"name",
                           @"9.2",@"rate"
                           , nil];
    [self.dataSource addObject:movie];
    
    // 2
    movie = [NSDictionary dictionaryWithObjectsAndKeys:
             @"The Godfather (1972)",@"name",
             @"9.2",@"rate"
             , nil];
    [self.dataSource addObject:movie];
    
    // 3
    movie = [NSDictionary dictionaryWithObjectsAndKeys:
             @"The Godfather: Part II (1974)",@"name",
             @"9.0",@"rate"
             , nil];
    [self.dataSource addObject:movie];
    
    // 4
    movie = [NSDictionary dictionaryWithObjectsAndKeys:
             @" The Dark Knight (2008)",@"name",
             @"8.9",@"rate"
             , nil];
    [self.dataSource addObject:movie];
    
    // 5
    movie = [NSDictionary dictionaryWithObjectsAndKeys:
             @"Pulp Fiction (1994)",@"name",
             @"8.9",@"rate"
             , nil];
    [self.dataSource addObject:movie];
    
    // 6
    movie = [NSDictionary dictionaryWithObjectsAndKeys:
             @"The Good, the Bad and the Ugly (1966)",@"name",
             @"8.9",@"rate"
             , nil];
    [self.dataSource addObject:movie];
    
    // 7
    movie = [NSDictionary dictionaryWithObjectsAndKeys:
             @"Schindler's List (1993)",@"name",
             @"8.9",@"rate"
             , nil];
    [self.dataSource addObject:movie];
    
    // 8
    movie = [NSDictionary dictionaryWithObjectsAndKeys:
             @"12 Angry Men (1957)",@"name",
             @"8.9",@"rate"
             , nil];
    [self.dataSource addObject:movie];
    
    // 9
    movie = [NSDictionary dictionaryWithObjectsAndKeys:
             @"The Lord of the Rings: The Return of the King (2003)",@"name",
             @"8.9",@"rate"
             , nil];
    [self.dataSource addObject:movie];
    
    // 10
    movie = [NSDictionary dictionaryWithObjectsAndKeys:
             @"Fight Club (1999)",@"name",
             @"8.8",@"rate"
             , nil];
    [self.dataSource addObject:movie];
    
    [self.tableView reloadData];
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UINib *nib = [UINib nibWithNibName:@"MovieTableViewCell" bundle:nil];
    [self.tableView registerNib:nib forCellReuseIdentifier:@"MovieTableViewCell"];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self fetchMovies];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 64.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MovieTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MovieTableViewCell"];
    
    NSDictionary *movie = [self.dataSource objectAtIndex:indexPath.row];
    cell.lblMovie.text = [movie objectForKey:@"name"];
    cell.lblNo.text = [NSString stringWithFormat:@"%ld",indexPath.row+1];
    cell.lblRate.text = [movie objectForKey:@"rate"];
    cell.poster.image = [UIImage imageNamed:[NSString stringWithFormat:@"%d.jpg",(int)indexPath.row+1]];
    
    return cell;
}


@end
