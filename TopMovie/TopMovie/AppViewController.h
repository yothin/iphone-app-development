//
//  AppViewController.h
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *dataSource;

@end
