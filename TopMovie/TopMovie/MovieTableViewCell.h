//
//  MovieTableViewCell.h
//  TopMovie
//
//  Created by Yothin Samrandee on 3/14/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MovieTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *poster;
@property (weak, nonatomic) IBOutlet UILabel *lblNo;
@property (weak, nonatomic) IBOutlet UILabel *lblMovie;
@property (weak, nonatomic) IBOutlet UILabel *lblRate;

@end
