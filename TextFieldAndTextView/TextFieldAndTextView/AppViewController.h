//
//  AppViewController.h
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppViewController : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UITextView *textVoew;
- (IBAction)hideKeyboardTapped:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *tfFirstname;
@property (weak, nonatomic) IBOutlet UITextField *tfLastname;
@property (weak, nonatomic) IBOutlet UITextField *tfEmail;
@property (weak, nonatomic) IBOutlet UITextField *tfPassword;
- (IBAction)submitTapped:(id)sender;

@end
