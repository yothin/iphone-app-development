//
//  AppViewController.m
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import "AppViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface AppViewController ()

@end

@implementation AppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.textVoew.layer.borderColor = [UIColor blackColor].CGColor;
    self.textVoew.layer.borderWidth = 1.0f;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)hideKeyboardTapped:(id)sender {
    
    [self.tfEmail resignFirstResponder];
    [self.tfFirstname resignFirstResponder];
    [self.tfLastname resignFirstResponder];
    [self.tfPassword resignFirstResponder];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
    }
    return YES;
}

- (IBAction)submitTapped:(id)sender {
    
    NSString *yourInfo = [NSString stringWithFormat:@"%@ %@ \nE-mail:%@ \nPassword:%@",self.tfFirstname.text,self.tfLastname.text,self.tfEmail.text,self.tfPassword.text];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Response" message:yourInfo delegate:Nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    
}
@end
