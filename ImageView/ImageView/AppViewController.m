//
//  AppViewController.m
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import "AppViewController.h"

@interface AppViewController ()

@end

@implementation AppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UIImage *image = [UIImage imageNamed:@"sakon-door.jpg"];
    self.localImageView.image = image;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonTapped:(id)sender {
    
    NSString *urlString = @"http://nattaporn33saytichai.files.wordpress.com/2011/09/e0b89ce0b8b2e0b981e0b894e0b887e0b899e0b8b2e0b887e0b984e0b8ade0b988.jpg";
    NSURL *url = [NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    
    self.remoteImageView.image = [UIImage imageWithData:data];
    
}

@end
