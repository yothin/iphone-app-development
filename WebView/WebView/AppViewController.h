//
//  AppViewController.h
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppViewController : UIViewController<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UITextField *tfURL;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
- (IBAction)goTapped:(id)sender;
- (IBAction)refresh:(UIBarButtonItem *)sender;
- (IBAction)next:(UIBarButtonItem *)sender;
- (IBAction)previous:(UIBarButtonItem *)sender;
- (IBAction)safari:(id)sender;

@end
