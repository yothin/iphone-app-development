//
//  AppViewController.m
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import "AppViewController.h"

@interface AppViewController ()

@end

@implementation AppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goTapped:(id)sender {
    
    // hide keyboard
    [_tfURL resignFirstResponder];

    NSString *urlString = self.tfURL.text;
    
    if (![urlString hasPrefix:@"http"]) {
        
        urlString = [NSString stringWithFormat:@"http://%@",urlString];
        
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:request];

}

- (IBAction)refresh:(UIBarButtonItem *)sender {
    [self.webView reload];
}

- (IBAction)next:(UIBarButtonItem *)sender {
    [self.webView goForward];
}

- (IBAction)previous:(UIBarButtonItem *)sender {
    [self.webView goBack];
}

- (IBAction)safari:(id)sender {
    
    NSString *urlString = self.tfURL.text;
    
    if (![urlString hasPrefix:@"http"]) {
        
        urlString = [NSString stringWithFormat:@"http://%@",urlString];
        
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    
    [[UIApplication sharedApplication] openURL:url];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self.activity stopAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.activity stopAnimating];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    [self.activity startAnimating];
}

@end
