//
//  AppViewController.m
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import "AppViewController.h"

@interface AppViewController ()

@end

@implementation AppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)stepper1Change:(id)sender {
    
    UIStepper *p = (UIStepper*)sender;

    _result1.text = [NSString stringWithFormat:@"%d",(int)p.value];
    
    [self calculateAverage];
}

- (IBAction)stepper2Change:(id)sender {
    
    UIStepper *p = (UIStepper*)sender;
    
    _result2.text = [NSString stringWithFormat:@"%d",(int)p.value];
    
    [self calculateAverage];
}

- (IBAction)stepper3Change:(id)sender {
    
    UIStepper *p = (UIStepper*)sender;
    
    _result3.text = [NSString stringWithFormat:@"%d",(int)p.value];
    
    [self calculateAverage];
}

-(void)calculateAverage{
    
    float result1 = [_result1.text floatValue];
    float result2 = [_result2.text floatValue];
    float result3 = [_result3.text floatValue];
    
    float average = (result1 + result2 + result3) / 3.0;
    _average.text = [NSString stringWithFormat:@"%0.2f",average];
}

@end
