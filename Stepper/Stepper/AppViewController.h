//
//  AppViewController.h
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *result1;
@property (weak, nonatomic) IBOutlet UILabel *result2;
@property (weak, nonatomic) IBOutlet UILabel *result3;
- (IBAction)stepper1Change:(id)sender;
- (IBAction)stepper2Change:(id)sender;
- (IBAction)stepper3Change:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *average;

@end
