//
//  AppViewController.m
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import "AppViewController.h"

@interface AppViewController ()

@end

@implementation AppViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    CGRect  f = _before.frame;
    f.origin.x = (self.view.frame.size.width - f.size.width ) / 2.0f;
    f.origin.y = 74.0f;
    _before.frame = f;
    
    f = _after.frame;
    f.origin.x = (self.view.frame.size.width - f.size.width ) / 2.0f;
    f.origin.y = 74.0f;
    _after.frame = f;

    
    [self.segmented setSelectedSegmentIndex:0];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segmentedChange:(id)sender {
    
    UISegmentedControl *segmented = (UISegmentedControl*)sender;
    
    if (segmented.selectedSegmentIndex == 0) {
        
        if (_before.superview) {
            return;
        }
        
        [_after removeFromSuperview];
        [self.view addSubview:_before];
        
    }else{
        
        if (_after.superview) {
            return;
        }
        
        [_before removeFromSuperview];
        [self.view addSubview:_after];
        
    }
    
    
}
@end
