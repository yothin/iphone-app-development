//
//  AppViewController.h
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIImageView *before;
@property (strong, nonatomic) IBOutlet UIImageView *after;
- (IBAction)segmentedChange:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmented;

@end
