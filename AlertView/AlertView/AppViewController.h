//
//  AppViewController.h
//  ActionSheet
//
//  Created by Yothin Samrandee on 3/13/2557 BE.
//  Copyright (c) 2557 Yothin Samrandee. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppViewController : UIViewController<UIAlertViewDelegate>
- (IBAction)oneButtonTapped:(id)sender;
- (IBAction)twoButtonTapped:(id)sender;
- (IBAction)threeButtonTapped:(id)sender;
- (IBAction)loginButtonTapped:(id)sender;
- (IBAction)hadlerUserTouchTapped:(id)sender;

@end
